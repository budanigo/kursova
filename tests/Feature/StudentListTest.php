<?php

namespace Tests\Feature;

use App\Models\Student;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class StudentListTest extends TestCase
{
    private $student;

    public function setUp(): void
    {
        parent::setUp();

        $this->student = new Student();
        $this->student->fio = 'Tatiana Volkova';
        $this->student->hostel = 15;
        $this->student->rating = 89.00;
        $this->student->scholarship = 1;
        $this->student->save();
    }

    public function test_Status()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
    }

    public function test_View()
    {
        $view = $this->view('unit', ['students' => [$this->student]]);
        $view->assertSeeText('Tatiana Volkova');
    }

    public function test_ViewWithMock()
    {
        $stub = $this->getMockBuilder(Student::class)->getMock();

        $stub->method('getStudent')
            ->willReturn(['id' => 1, 'fio' => 'Test Test', 'hostel' => 1, 'rating' => 60, 'scholarship' => 0]);

        $this->assertContains('Test Test', $stub->getStudent(null));
        $this->assertContains(60, $stub->getStudent(null));

        $view = $this->view('unit', ['students' => [(object)$stub->getStudent(1)]]);
        $view->assertSeeText('Test Test');
    }

    public function test_SeeInDB()
    {
        $student = Student::query()->where('fio', '=', 'Tatiana Volkova')->first();

        $this->assertEquals('Tatiana Volkova', $student->fio);
        $this->assertEquals(0, $student->scholarship);
    }

    public function tearDown(): void
    {
        parent::tearDown();
        $this->student->delete();
    }
}
