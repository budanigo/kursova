<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Illuminate\Http\Request;

class GuestController extends Controller
{
    public function index(Request $request){
        return view('guest.index', ['request' => $request]);
    }

    public function abit(Request $request){
        return view('guest.abit', ['request' => $request]);
    }

    public function info(Request $request){
        return view('guest.info', ['request' => $request]);
    }

    public function unit(){
        return view('unit', ['students' => Student::all()]);
    }
}
