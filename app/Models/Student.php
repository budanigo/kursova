<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;

    const HOSTELS = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];

    protected $fillable = [
        'id',
        'fio',
        'hostel',
        'rating',
        'scholarship',
    ];

    public static function getStudents($request){
        $students = Student::query();
        if ($request->filled('rating'))
            $students->where('rating','=', $request->rating);

        if ($request->filled('hostel'))
            $students->where('hostel','=', $request->hostel);

        if ($request->filled('sort')){
            switch ($request->sort){
                case 'fio':
                    $students->orderBy('fio');
                    break;
                case 'hostel':
                    $students->orderByDesc('hostel');
                    break;
                case 'rating':
                    $students->orderByDesc('rating');
                    break;
                case 'scholarship':
                    $students->orderByDesc('scholarship');
                    break;
            }
        }
        return $students->get();
    }

    public function getStudent($id){
        return Student::query()->find($id)->get();
    }

    protected $table = 'students';
    public $timestamps = false;
}
