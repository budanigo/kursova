<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\GuestController;
use App\Http\Controllers\AdminController;

Route::get('/', [GuestController::class, 'index'])->name('index');
Route::get('/abit', [GuestController::class, 'abit'])->name('abit');
Route::get('/info', [GuestController::class, 'info'])->name('info');

Route::get('/unit', [GuestController::class, 'unit'])->name('unit');


Route::get('/admin', function (){return redirect('/admin/students');});

Route::resource('/admin/students', AdminController::class);
Auth::routes();

