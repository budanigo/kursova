@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">
                        <h5 class="text-center">Редагування студента</h5>
                    </div>
                    <div class="card-body">
                        <form action="/admin/students/{{$student->id}}" method="post">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label>ФІО: </label>
                                <input type="text" class="form-control" name="fio" placeholder="Фіо"
                                       value="{{$student->fio}}" required>
                            </div>
                            <br>

                            <span>Гуртожиток:
                                <select class="form-control" name="hostel" required>
                                    @foreach(\App\Models\Student::HOSTELS as $hostel)
                                        <option value="{{$hostel}}" {{($student->hostel == $hostel) ? 'selected' : ''}}>
                                            №{{$hostel}}
                                        </option>
                                    @endforeach
                                </select>
                            </span>
                            <br>

                            <div class="form-group">
                                <label>Рейтинг: </label>
                                <input type="number" class="form-control" name="rating" placeholder="Рейтинг"
                                       value="{{$student->rating}}" required>
                            </div>
                            <br>

                            <div class="form-group">
                                <span>Отримує стипендію?: </span>
                                <input type="radio" id="scholarship_yes" name="scholarship" value="1"
                                    {{($student->scholarship == 1) ? 'checked' : ''}}>
                                <label for="scholarship_yes">Так</label>
                                <input type="radio" id="scholarship_no" name="scholarship" value="0"
                                    {{($student->scholarship == 0) ? 'checked' : ''}}>
                                <label for="scholarship_no">Ні</label>
                            </div>
                            <br>

                            <input type="submit" class="btn btn-success form-control" value="Зберегти">
                        </form>
                        <a href="/admin" class="link-danger"><div class="text-center">Повернутись назад</div></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
