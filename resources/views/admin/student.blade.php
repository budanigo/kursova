@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h5 class="text-center">{{$student->fio}}</h5>
                    </div>
                    <div class="card-body">
                        <p>ФІО: {{$student->fio}}</p>
                        <p>Проживає в гуртожитку №{{$student->hostel}}</p>
                        <p>Рейтинг: {{$student->rating}}/100</p>
                        <p>Стипендіат: {{($student->scholarship == 1) ? 'Так' : 'Ні'}}</p>
                        <a href="/admin/students" class="link-danger"><div class="text-center">Повернутись назад</div></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
