@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <form>
                    <div class="row">
                        <div class="col">
                            <input class="form-control" type="number" name="rating" value="{{$request->rating ?: '' }}"
                                   placeholder="Рейтинг">
                        </div>
                        <div class="col">
                            <input class="form-control" type="number" name="hostel" value="{{$request->hostel ?: '' }}"
                                   placeholder="Гуртожиток">
                        </div>
                        <div class="col">
                            <input class="btn btn-success form-control" type="submit" value="Пошук">
                        </div>
                        <div class="col">
                            <a href="/admin/students" class="btn btn-warning form-control">Скинути</a>
                        </div>
                        <div class="col">
                            <a href="/admin/students/create" class="btn btn-info form-control">Додати</a>
                        </div>
                    </div>
                </form>

                <table class="table mt-4">
                    <tr>
                        <td><a href="/admin/students?sort=fio">Фіо</a></td>
                        <td><a href="/admin/students?sort=hostel">Гуртожиток</a></td>
                        <td><a href="/admin/students?sort=rating">Рейтинг</a></td>
                        <td><a href="/admin/students?sort=scholarship">Стипендія</a></td>
                        <td colspan="2" class="text-info">Керування</td>
                    </tr>
                    @foreach($students as $student)
                        <tr>
                            <td><a href="/admin/students/{{$student->id}}">{{$student->fio}}</a></td>
                            <td>{{$student->hostel}}</td>
                            <td>{{$student->rating}}</td>
                            <td>{{$student->scholarship ? 'Так' : 'Ні'}}</td>
                            <td style="max-width: 65px"><a href="/admin/students/{{$student->id}}/edit" class="btn btn-sm btn-warning">Редагувати</a></td>
                            <td style="max-width: 65px">
                                <form action="/admin/students/{{$student->id}}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" class="btn btn-sm btn-danger" value="Видалити">
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection
