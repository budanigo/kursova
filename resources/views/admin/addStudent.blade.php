@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">
                        <h5 class="text-center">Додати студента</h5>
                    </div>
                    <div class="card-body">
                        <form action="/admin/students" method="post">
                            @csrf
                            <div class="form-group">
                                <label>ФІО: </label>
                                <input type="text" class="form-control" name="fio" placeholder="Фіо" required>
                            </div>
                            <br>

                            <span>Гуртожиток:
                                <select class="form-control" name="hostel" required>
                                    @foreach(\App\Models\Student::HOSTELS as $hostel)
                                        <option value="{{$hostel}}">№{{$hostel}}</option>
                                    @endforeach
                                </select>
                            </span>
                            <br>

                            <div class="form-group">
                                <label>Рейтинг: </label>
                                <input type="number" class="form-control" name="rating" placeholder="Рейтинг" required>
                            </div>
                            <br>

                            <div class="form-group">
                                <label>Отримує стипендію?: </label>
                                <input type="radio" id="scholarship_yes" name="scholarship" value="1">
                                <label for="scholarship_yes">Так</label>
                                <input type="radio" id="scholarship_no" name="scholarship" value="0">
                                <label for="scholarship_no">Ні</label>
                            </div>
                            <br>

                            <input type="submit" class="btn btn-success form-control" value="Додати">
                        </form>
                        <a href="/admin" class="link-danger"><div class="text-center">Повернутись назад</div></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
