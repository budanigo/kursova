<table border="1">
    <tr>
        <td>Фіо</td>
        <td>Гуртожиток</td>
        <td>Рейтинг</td>
        <td>Стипендія</td>
    </tr>
    @foreach($students as $student)
        <tr>
            <td>{{$student->fio}}</td>
            <td>{{$student->hostel}}</td>
            <td>{{$student->rating}}</td>
            <td>{{$student->scholarship ? 'Так' : 'Ні'}}</td>
        </tr>
    @endforeach
</table>
