@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header text-center"><h2>День відкритих дверей</h2></div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <p>Київський Політехнічний університет</p>
                        <p>Інженерно-Хімічний Факультет</p>
                        <i>ДНІ ВІДКРИТИХ ДВЕРЕЙ:</i>
                        <p>19.12.2020 р. Початок о 15.00</p>
                        <p>06.02.2021 р. Початок о 15.00</p>
                        <p>20.03.2021 р. Початок о 15.00</p>
                        <p>17.04.2021 р. Початок о 15.00</p>
                        <p>Посилання на онлайн-конференцію:
                            <a href="#">
                                https://us02web.zoom.us/j/6581015116?pwd=MStGVHdMcUNKMWdiZkF2RggEveUVVUT09
                            </a>
                        </p>
                        <p>Ідентифікатор: 658 666 5116</p>
                        <p>Пароль: 170421</p>
                        <p>Телефони для довідок:</p>
                        <p>+38 (044) 999 99 99, +38 (097) 999 99 99, +38 (067) 999 99 99</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
