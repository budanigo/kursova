@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <h1>Інформаційна система "Деканат"</h1>
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
        </div>
        <h2>Новини:</h2>
        <div class="row mt-2">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header text-center"><h3>Новина №1</h3></div>
                    <div class="card-body">
                        Певна інформація
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-2">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header text-center"><h3>Новина №2</h3></div>
                    <div class="card-body">
                        Певна інформація
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-2">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header text-center"><h3>Новина №3</h3></div>
                    <div class="card-body">
                        Певна інформація
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
